import React, { useEffect, useState  } from 'react';
import './App.css';

function App() {
  //API authentication
  //const API_KEY  = "d5996fab-c96b-48e0-8545-aeea41e9f7b2";

  const [ userInfo, setUserInfo ] = useState ([]);

  useEffect(() => {
    getUserInfo();
  }, []);

  const getUserInfo = async () => { 
    const response = await fetch("https://api.fortnitetracker.com/v1/profile/pc/pedrinhoTsunami", {  "headers": {"TRN-Api-Key": "65a99cc9-2ce9-4574-8ad8-ea5d5094aa7d" }})
    const data = await response.json();
    console.log(data);
  }
//   async getDataFetch(){ const response = await fetch("https://dog.ceo/api/breeds/list/all", { headers: {'Content-Type': 'application/json'}})
//     console.log(await response.json())
// }


  return (
    <div className="App">
      <form className = "search-form">
        <h3>Procurar Jogador : </h3>
        <h4>Plataforma : </h4>
        <input className = "plataforma-input"/> {/*Change to select later*/}
        <h4>Nickname : </h4>
        <input className = "nickname-input"/>
        <button className = "search-btt"> Procurar</button>
      </form>
      
    </div>
  );
}

export default App;
